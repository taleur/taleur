/*
 * Load flv stream from http,
 * save 20 minutes to mp3 file,
 * id3 tag it and
 * generate new RSS feed
 * hardcoded to retrieve http://2783.live.streamtheworld.com/CHMPFM
 */

var fs			= require('fs'),
	http		= require('http'),
	flv			= require('flv'),
	flvee		= require('flvee'),
	ffmetadata	= require('ffmetadata'), // -codec => -acodec, id2v3...
	util		= require('util'),
    xml2js		= require('xml2js'),
	tsecs		= 20*60;

go();
setInterval(go, tsecs*1000);

function go() {
	console.log('Fetching...');
	http.get('http://2783.live.streamtheworld.com/CHMPFM', function(res) {
		var	decoder		= new flv.Decoder(),
			parser		= new flvee.Parser(),
			now			= Date.now(),
			now_d		= new Date(now),
			now_str		= now_d.toString(),
			minutes		= 0,
			last_minutes= 0,
			filename	= 'chmp-' + now + '.mp3',
			tags		= {
				artist: 'cogeco chmp 98.5 fm',
				genre: 12, // 101 speech (extended) or 12 for other
				comment: 'via http://www.taleur.com/',
				title: 'Enregistrement de ' + now_str.slice(16),
				album: 'Enregistrements du ' + now_str.slice(0, 15),
				year: now_d.getFullYear(), date: now_d.getFullYear(), // not showing up..?
				track: Math.floor((now_d.getHours() * 60 + now_d.getMinutes())/20) + '/72' // 21:40, counting in 0:20 from 0:00
			},
			item		= {
				title: [ 'CHMP ' + now_str ],
				link: [ 'http://www.taleur.com/' ], // va faire pour le moment
				author: [ tags.artist ],
				enclosure: [{
					'$': {
						url: 'http://audio.taleur.com/' + filename,
						type: 'audio/mpeg'
					}
				}],
				guid: [ 'http://audio.taleur.com/' + filename ],
				pubDate: [ now_str ], // TODO get real date time
				description: [ tags.comment ] // copie de tags.comment
			};
			
		res.on('close', function() {
			ffmetadata.write('../public/' + filename, tags, function(err) {
				if (err) {
					console.error("Error writing metadata", err);
				} else {
					console.log("Wrote metadata to " + filename);
					// let's generate rss feed (with enclosure) now
					// need the filesize
					fs.stat('../public/' + filename, function(err, stats) {
						if (err) {
							console.error("Error getting filesize", err);
						} else {
							item.enclosure[0]['$'].length = stats.size;
							woot(item);
						}
					});
				}
			});		
		});

		res.pipe(parser);
		res.pipe(decoder);

		parser.on("readable", function() {
		  var e;
		  while (e = parser.read()) {
			if (e.packet && (8 === e.packet.type)) {
				minutes = Math.floor(e.packet.time/60000);
				 
				if (minutes > last_minutes ) {
					console.log('Minute ' + minutes);
					last_minutes = minutes;
				}
				
				if (e.packet.time > tsecs * 1000 + 6500 ) {
				  console.log('ENDING!!');
				  res.emit('end');
				  return;
				}
			}
		  }
		});

		decoder.on('audio', function (audio) {
		  audio.pipe(fs.createWriteStream('../public/' + filename));
		});
	});
}

function woot(item) {
	var now = Date.now(), now_keep = now - 1000 * (86400 + 43200), now_str = new Date(now).toString();
	
	fs.readFile('../public/rss3.xml', function(err, data) {
		var builder = new xml2js.Builder(), rss, parser = new xml2js.Parser();

		if (err) {
			rss = {
				rss: {
					'$': { version: '2.0' },
					channel: [{
							title: 'CHMP 98.5 FM Montréal',
							item: [ item ]
					}]
				}
			};
		} else {
			parser.parseString(data, function (err, result) {
				// only keep recent items
				result.rss.channel[0].item = result.rss.channel[0].item.filter(function (i) {
					if (i.pubDate) return Date.parse(i.pubDate) >  now_keep;
					return false;
				});
				result.rss.channel[0].item.push(item);
				rss = result;
			});
		}

		// format and write rss
		fs.writeFile('../public/rss3.xml', builder.buildObject(rss), function (err) {
			if (!err) console.log('Feed saved');
		});
	});
}
